# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

# Copyright 2020 KappaZeta Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score

# Make it possible to run the script headless (with $DISPLAY undefined).
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import utility.log as ulog
import utility.csv as ucsv


class RITA_Eval(ulog.Loggable):
    """Evaluate prediction results"""

    # Crop class IDs and names to use for plotting the confusion matrix.
    CLASSES = [
        (1, "Aedmaasikas"),
        (2, "Heintaimed, kõrrelised"),
        (3, "Liblikõieliste segud (alla 80%)"),
        (4, "Liblikõielised (üle 80%)"),
        (5, "Kanep"),
        (6, "Kartul"),
        (7, "Põldhernes"),
        (8, "Põlduba"),
        (9, "Muu kaunvili"),
        (10, "Mais"),
        (11, "Astelpaju"),
        (12, "Marjapõõsad ja viljapuud"),
        (13, "Mustkesa"),
        (14, "Peakapsas"),
        (15, "Porgand"),
        (16, "Punapeet"),
        (17, "Suviraps ja -rüps"),
        (18, "Suvinisu ja speltanisu"),
        (19, "Suvioder"),
        (20, "Kaer"),
        (21, "Tatar"),
        (22, "Taliraps ja -rüps"),
        (23, "Rukis"),
        (24, "Talinisu"),
        (25, "Talioder"),
        (26, "Talitritikale"),
        (27, "Muu teravili"),
        (28, "Muu"),
        (101, "Aedmaasikas"),
        (102, "Heintaimed, kõrrelised"),
        (103, "Heintaimed, liblikõielised"),
        (104, "Kanep"),
        (105, "Kartul"),
        (106, "Kaunviljad"),
        (107, "Mais"),
        (108, "Marjapõõsad, viljapuud ja astelpaju"),
        (109, "Mustkesa"),
        (110, "Peakapsas"),
        (111, "Rühvelkultuurid"),
        (112, "Suviraps ja -rüps"),
        (113, "Suviteraviljad"),
        (114, "Taliraps ja -rüps"),
        (115, "Taliteraviljad"),
        (116, "Muu")
    ]

    def __init__(self, path_output):
        super(RITA_Eval, self).__init__("RITA.E.Eval")

        self.path_output = path_output

        self.overall_accuracy = 0
        self.khat = 0
        self.overall_f1score = 0

    def get_confusion_matrix(self, y_true, y_pred):
        """
        Calculate confusion matrix (and normalized confusion matrix) from the actual (y_true) and predicted (y_pred) values.
        """
        self.overall_f1 = f1_score(y_true, y_pred, average="weighted")
        self.overall_accuracy = accuracy_score(y_true, y_pred)
        self.log.info("Weighted F1 score: {}".format(self.overall_f1))

        # Compute confusion matrix
        cm = confusion_matrix(y_true, y_pred)
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        self.khat = self.calculate_KHAT(cm)
        #self.log.info("KHAT: {}".format(self.khat))

        return cm, cm_normalized

    def get_metrics_perclass(self, y_true, y_pred):
        precision = precision_score(y_true, y_pred, average=None)
        recall = recall_score(y_true, y_pred, average=None)
        f1score = f1_score(y_true, y_pred, average=None)

        return precision, recall, f1score

    def get_metrics_average(self, y_true, y_pred):
        precision = precision_score(y_true, y_pred, average='weighted')
        recall = recall_score(y_true, y_pred, average='weighted')
        f1score = f1_score(y_true, y_pred, average='weighted')

        return precision, recall, f1score

    def save_metrics_perclass(self, precision, recall, f1score, path_output):
        df = pd.DataFrame(list(zip(precision, recall, f1score)), columns=['Precision', 'Recall', 'F1'])
        df = df.round(2)
        # Incrementing for custom classes purposes
        df.index = range(1, len(df) + 1)
        classes_df = pd.DataFrame.from_dict(dict(self.CLASSES), orient='index', columns=["Crop type"])
        df = classes_df.merge(df, how="inner", left_index=True, right_index=True)
        df.to_csv(path_output + '/per_class_statistics.csv', index=True, header=True)
        return

    def save_metrics_average(self, precision, recall, f1score, path_output):
        with open(path_output + '/weighted_statistics.csv', "wt") as fo:
            fo.write(
                (
                    "metric,value\n"
                    "Precision,{}\n"
                    "Recall,{}\n"
                    "F1 score,{}\n"
                ).format(precision, recall, f1score)
            )
        return

    def filter_fids(self, dataframe, fid_filter_path):
        with open(fid_filter_path) as fo:
            fid_list = fo.read()
            filtered_fid_list = fid_list.rstrip('\n').split(",")

        selected_fid = dataframe[dataframe['f_id'].isin(filtered_fid_list)]
        no_fid = dataframe[~dataframe['f_id'].isin(filtered_fid_list)]

        filtered_statistic = selected_fid.groupby(['actual']).count()["f_id"].to_dict()
        for key, value in filtered_statistic.items():
            if value < 10:
                samles_to_add = 10 - value
                new_rows = no_fid.loc[no_fid['actual'] == key].sample(n=samles_to_add)
                selected_fid = selected_fid.append(new_rows)

        return selected_fid

    def get_statistics(self, filtered_df):
        correct_prediction = filtered_df.loc[filtered_df["actual"] == filtered_df["prediction"]]
        correct_count = correct_prediction.groupby("actual")["f_id"]\
            .count().reset_index()\
            .rename(columns={'f_id': 'correct_count'})
        f_idcorrect = correct_prediction.groupby("actual")["f_id"]\
            .apply(list).reset_index()\
            .rename(columns={'f_id': 'correct_fid'})
        correct_statistic_df = pd.merge(correct_count, f_idcorrect, on="actual")
        errors_prediction = filtered_df.loc[filtered_df["actual"] != filtered_df["prediction"]]
        predicted_as = errors_prediction.groupby("actual")["prediction"] \
            .apply(list).reset_index() \
            .rename(columns={'prediction': 'predicted_as'})
        errors_count = errors_prediction.groupby("actual")["f_id"]\
            .count().reset_index()\
            .rename(columns={'f_id': 'errors_count'})
        f_iderorrs = errors_prediction.groupby("actual")["f_id"]\
            .apply(list).reset_index()\
            .rename(columns={'f_id': 'errors_fid'})
        error_statistics_df = pd.merge(errors_count, f_iderorrs, on="actual")
        error_statistics_df = pd.merge(error_statistics_df, predicted_as, on="actual")

        final_statistics = pd.merge(correct_statistic_df, error_statistics_df, on="actual", how="outer")

        return final_statistics

    def plot_confusion_matrix(self, cm_normalized, class_list, title, normalized=False, cmap=plt.cm.Blues):
        """
        Print and plot the confusion matrix.
        Normalization can be applied by setting `normalized=True`.

        Based on https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py

        :param cm_normalized: Normalized confusion matrix
        :param class_list: List of classnames
        :param title: Figure caption
        :param normalized: Apply normalization (enabled by default)
        :param cmap: Colormap for the table tiles
        :return: Figure axis (could be used to draw overlays)
        """
        fig, ax = plt.subplots(figsize=(20, 20))
        im = ax.imshow(cm_normalized, interpolation='nearest', cmap=cmap)
        # We want to show all ticks...
        ax.set(xticks=np.arange(cm_normalized.shape[1]),
               yticks=np.arange(cm_normalized.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=class_list, yticklabels=class_list)
        plt.xlabel('Predicted label', fontsize=20)
        plt.ylabel('True label', fontsize=20)
        ax.set_title(title, pad=30, fontsize=21)

        # Turn spines off.
        for edge, spine in ax.spines.items():
            spine.set_visible(False)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor", fontsize=20)
        plt.setp(ax.get_yticklabels(), fontsize=20)

        # Loop over data dimensions and create text annotations.
        fmt = '.2f' if normalized else 'd'
        thresh = cm_normalized.max() / 2.
        for i in range(cm_normalized.shape[0]):
            for j in range(cm_normalized.shape[1]):
                color = "white" if cm_normalized[i, j] > thresh or cm_normalized[i, j] < 0.01 else "black"
                ax.text(j, i, format(cm_normalized[i, j], fmt),
                        ha="center", va="center",
                        color=color, fontsize=15)
        fig.tight_layout()
        return ax

    def evaluate_crop(self, dt, filter_path=False):
        """
        Evaluate the accuracy of crop class estimates
        :param dt: Pandas DataFrame with proba_X and actual_X columns, where X corresponds to the class index
        """
        columns_proba = [col for col in dt.columns if col.startswith("proba_")]
        columns_actual = [col for col in dt.columns if col.startswith("actual_")]
        idxs_proba = [int(re.match(r'proba_(\d+)', c).group(1)) for c in columns_proba]
        idxs_actual = [int(re.match(r'actual_(\d+)', c).group(1)) for c in columns_actual]

        y_pred = np.argmax(dt[columns_proba].values, axis=1)
        y_actual = np.argmax(dt[columns_actual].values, axis=1)
        y_pred = np.take(idxs_proba, y_pred)
        y_actual = np.take(idxs_actual, y_actual)

        # Store predictions (both probabilities for each class, as well as the class with the highest probability).
        ucsv.csv_write("results/predictions_raw.csv", dt, overwrite=True)
        dt.loc[:, "prediction"] = y_pred
        dt.loc[:, "actual"] = y_actual
        dt.loc[:, "prediction_name"] = dt["prediction"]
        dt.loc[:, "actual_name"] = dt["actual"]
        dt.replace({"prediction_name": dict(self.CLASSES), "actual_name": dict(self.CLASSES)}, inplace=True)
        # Filtering subset of parcels f_ids
        if filter_path:
            filtered = self.filter_fids(dt, filter_path)
            stats_filter = self.get_statistics(filtered)
            filtered_correct = filtered.loc[filtered["actual"] == filtered["prediction"]]
            filtered_errors = filtered.loc[filtered["actual"] != filtered["prediction"]]
            ucsv.csv_write(
                "results/filtered_results.csv",
                filtered[["parcel_id", "f_id", "prediction", "actual", "prediction_name", "actual_name"]],
                overwrite=True
            )
            ucsv.csv_write(
                "results/filtered_correct.csv",
                filtered_correct[["parcel_id", "f_id", "prediction", "actual", "prediction_name", "actual_name"]],
                overwrite=True
            )
            ucsv.csv_write(
                "results/filtered_errors.csv",
                filtered_errors[["parcel_id", "f_id", "prediction", "actual", "prediction_name", "actual_name"]],
                overwrite=True
            )
            ucsv.csv_write(
                "results/filtered_statistics.csv",
                stats_filter[["actual", "correct_count", "errors_count", "predicted_as", "correct_fid", "errors_fid"]],
                overwrite=True
            )

        ucsv.csv_write(
            "results/predictions.csv",
            dt[["parcel_id", "f_id", "prediction", "actual", "prediction_name", "actual_name"]],
            overwrite=True
        )

        class_list = ['{}. {}'.format(code, name) for code, name in self.CLASSES if code in y_actual]

        # Calculate the confusion matrix.
        cm, cm_normalized = self.get_confusion_matrix(y_actual, y_pred)

        precision_list, recall_list, f1score_list = self.get_metrics_perclass(y_actual, y_pred)
        self.save_metrics_perclass(precision_list, recall_list, f1score_list, self.path_output)

        self.precision, self.recall, self.f1score = self.get_metrics_average(y_actual, y_pred)
        self.save_metrics_average(self.precision, self.recall, self.f1score, self.path_output)

        # Convert the confusion matrix into Pandas DataFrames.
        df_cm = pd.DataFrame(data=cm, columns=class_list)
        df_cm.insert(0, "class", class_list)
        df_cm_normalized = pd.DataFrame(data=cm_normalized, columns=class_list)
        df_cm_normalized.insert(0, "class", class_list)
        # Save confusion matrix into CSV files.
        ucsv.csv_write(os.path.join(self.path_output, 'confusion_matrix.csv'), df_cm, True)
        ucsv.csv_write(os.path.join(self.path_output, 'confusion_matrix_normalized.csv'), df_cm_normalized, True)
        # Plot the normalized confusion matrix.
        self.plot_confusion_matrix(cm_normalized, class_list, "RITA-Evaluator confusion matrix", normalized=True)
        plt.savefig(os.path.join(self.path_output, 'confusion_matrix_plot.png'))
        plt.close()

    def calculate_KHAT(self, cm):
        """
        Calculate KHAT metric from a confusion matrix
        :param cm: Confusion matrix (not normalized)
        :return: KHAT metric in range [0, 1]
        """
        df_elements_sum = cm.sum()
        df_norm = cm / df_elements_sum
        p0 = np.diag(df_norm).sum()
        columns_sum = df_norm.sum(axis=0)
        rows_sum = df_norm.sum(axis=1)
        pc = (columns_sum * rows_sum).sum()
        K = (p0-pc)/(1-pc)
        return K

    def save_statistics(self, path):
        """
        Save statistics to CSV
        :param path: Path to the CSV file
        """
        with open(path, "wt") as fo:
            fo.write(
                (
                    "metric,value\n"
                    "overall_accuracy,{}\n"
                    "khat,{}\n"
                ).format(self.overall_accuracy, self.khat)
            )

    def save_filtered_statistics(self, path, filterid_list):
        """
        Save statistics to CSV
        :param path: Path to the CSV file
        """
        with open(path, "wt") as fo:
            fo.write(
                (
                    "metric,value\n"
                    "overall_accuracy,{}\n"
                    "khat,{}\n"
                ).format(self.overall_accuracy, self.khat)
            )
