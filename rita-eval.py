#!/usr/bin/python3
# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

# Copyright 2020 KappaZeta Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gc
import argparse
import numpy as np
import pandas as pd

import processing.timeseries as pts
import processing.nn_model as pnn
import evaluation.confusion_matrix as ecm
import utility.log as ulog


def main():
    # Parse command-line arguments.
    p = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

    p.add_argument("-i", "--input", action="store", dest="path_input", default="data/timeseries.csv",
                   help="Path to the time series dataset.")
    p.add_argument("-m", "--meta", action="store", dest="path_metadata", default="model/metadata.json",
                   help="Path to the model metadata JSON.")
    p.add_argument("-w", "--weights", action="store", dest="path_weights", default="model/model.hdf5",
                   help="Path to the model weights.")
    p.add_argument("-n", "--num-parcels", action="store", dest="num_parcels", default=1000,
                   help="Number of parcels to predict at once or -1 to predict all at once.")
    # p.add_argument("-j", "--jobs", dest="num_workers", action="store", default=4,
    #                help="Number of workers to spawn for result table processing")
    p.add_argument("-l", "--log", action="store", dest="logfile_path", default=None,
                   help="Path for a log file, if desired.")
    p.add_argument("-v", "--verbosity", dest="verbosity", type=int, action="store", default=3,
                   help="Level of verbosity (1 - 3).")
    p.add_argument("-filter", "--filter", required=False, action="store", dest="fid_filter", default="data/filter_fid.txt",
                   help="F_id of specific interest.")

    args = p.parse_args()

    log = None
    try:
        # Initialize logging.
        log = ulog.init_logging(
            args.verbosity, "RITA evaluator", "RITA.E",
            logfile=args.logfile_path
        )

        # Initialize the model.
        nn = pnn.RITA_NN_Model()
        nn.load_metadata(args.path_metadata)
        nn.construct()
        nn.load_weights(args.path_weights)

        # Initialize time series processing.
        ts = pts.RITA_TS()
        ts.set_input_path(args.path_input)
        ts.set_doy_range(nn.ts_doy_min, nn.ts_doy_max)
        ts.set_features(
            nn.features_to_interpolate, nn.nofill_features, nn.indicator_features,
            nn.label_column_names, nn.column_names
        )

        # Column names for estimates and actual labels.
        cols_proba = ["proba_{}".format(i) for i in nn.indicator_features["label"]]
        cols_actual = ["actual_{}".format(i) for i in nn.indicator_features["label"]]

        # Iteratively process the time series and predict.
        preds = []
        for i, n, p_ids, f_ids, x, y in ts.go(args.num_parcels):
            np_preds = nn.predict(x)

            # Combine the input data with prediction results and labels for analysis.
            dt_pred = pd.DataFrame(
                data=np.concatenate([np_preds, y], axis=1),
                columns=(cols_proba + cols_actual)
            )
            dt_pred = pd.concat([pd.Series(p_ids, name="parcel_id"), dt_pred], axis=1)
            dt_pred = pd.concat([pd.Series(f_ids, name="f_id"), dt_pred], axis=1)

            preds.append(dt_pred)
        dt_preds = pd.concat(preds)

        # Join parcel_id and f_id

        # Evaluate the accuracy of the predictions.
        eval = ecm.RITA_Eval("results/")
        if args.fid_filter:
            eval.evaluate_crop(dt_preds, args.fid_filter)
        else:
            eval.evaluate_crop(dt_preds)
        # Change this file
        #eval.save_statistics("results/statistics.csv")

    except Exception as e:
        if log is not None:
            log.exception("Unhandled exception")
        else:
            print("Failed to initialize error logging")
            raise e


if __name__ == "__main__":
    main()

gc.collect()
