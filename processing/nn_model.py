# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

# Copyright 2020 KappaZeta Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import tensorflow as tf

import utility.log as ulog


class RITA_NN_Model(ulog.Loggable):
    """TensorFlow model"""

    def __init__(self):
        super(RITA_NN_Model, self).__init__("RITA.E.MODEL")

        # Data parameters.
        self.ts_doy_min = 0
        self.ts_doy_max = 0

        self.model = None
        self.model_base = None
        self.model_architecture = None
        self.model_opt = None

        self.column_names = []
        self.label_column_names = []

        # Default values, in the case that these are missing from the model metadata JSON.
        self.features_to_interpolate = [
            'cohvv_median', 'cohvh_median', 'vhvv_median', 's0vv_median', 's0vh_median', 'b3_median', 'b4_median',
            'b6_median', 'b8a_median', 'b11_median', 'b12_median', 'ndvi_median', 'ndwi_median', 'ndvire_median',
            'tc_vegetation_median', 'misra_yellow_vegetation_median', 'psri_median', 'wri_median',
            'precipitation_sum_mean_3h', 'precipitation_sum_mean_24h', 'temperature_mean'
        ]
        self.soil_types1 = ['A', 'B', 'C', 'MA']
        self.soil_types4 = [
            '10', '21', '13', '48', '109', '73', '45', '61', '14', '44', '64', '51', '53', '77', '31', '11', '43',
            '16', '0', '42', '63', '37', '57'
        ]
        self.indicator_features = {
            'soil_type': self.soil_types4,
            'label': [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28
            ]
        }
        self.nofill_features = []

    def __del__(self):
        # TensorFlow leaks memory without explicitly clearing the session.
        tf.keras.backend.clear_session()

    def validate_model(self, model):
        """
        Validate a model and its compatibility to the shape of input data.
        Raise AssertionError when an incompatibility is detected.

        :param model: TensorFlow Keras model to validate
        """
        # Verify the naming convention.
        l_input = model.get_layer("main_input")
        l_output = model.get_layer("outcome_output")
        assert l_input is not None, "Expecting an input layer 'main_input' in model architecture"
        assert l_output is not None, "Expecting an output layer 'outcome_output' in model architecture"

    def construct(self):
        """Construct the the model."""
        self.log.info("Constructing model..")

        self.log.info("Using TensorFlow {}".format(tf.__version__))

        self.model = tf.keras.models.model_from_json(json.dumps(self.model_architecture))
        self.validate_model(self.model)

    def load_weights(self, path):
        """
        Load model weights
        :param path: Path to the HDF5 file with weights
        """
        self.model.load_weights(path)

    def predict(self, x):
        """
        Predict on x
        :param x: Input tensor as Numpy ndarray
        :return: Numpy array of probabilities
        """
        return self.model.predict(x)

    def load_metadata(self, path):
        """
        Load model architecture and parameters for time series processing from a metadata file
        :param path: Path to the metadata JSON file
        """
        with open(path, "rt") as fi:
            metadata = json.load(fi)

            # While the train section may contain metadata from multiple models due to
            # Cross-Validation, the following parameters should be identical for all of them.
            # Thus, we'll just take the parameters of the first model.
            section_nn = metadata["train"][0]["nn"]

            section_create = metadata["create_data_train"]
            self.ts_doy_min = section_create["data"]["ts_doy_min"]
            self.ts_doy_max = section_create["data"]["ts_doy_max"]

            self.model_base = section_nn["model"]["model"]
            self.model_architecture = section_nn["model"]["arch"]

            section_data = metadata["train"][0]["data"]
            self.column_names = section_data["column_names"]
            self.label_column_names = section_data["label_cols"]
            if "features_to_interpolate" in section_data.keys():
                self.features_to_interpolate = section_data["features_to_interpolate"]
            if "indicator_features" in section_data.keys():
                self.indicator_features = section_data["indicator_features"]
