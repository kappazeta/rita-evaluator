# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

# Copyright 2020 KappaZeta Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import pandas as pd

import utility.log as ulog


class RITA_TS(ulog.Loggable):
    """Process time series"""

    def __init__(self):
        super(RITA_TS, self).__init__("RITA.E.TS")

        self.path_input = ""

        self.features_to_interpolate = []
        self.nofill_features = []
        self.indicator_features = {}
        self.label_columns = []
        self.include_features = []

        self.ts_doy_min = 0
        self.ts_doy_max = 0

    def set_input_path(self, path):
        """
        Set time series input path
        :param path: Path to the CSV file with header
        """
        self.path_input = path

    def set_doy_range(self, doy_min, doy_max):
        """
        Limit time series to a range of DoY
        :param doy_min: Minimum DoY to include in the time series
        :param doy_max: Maximum DoY to include in the time series
        """
        self.ts_doy_min = doy_min
        self.ts_doy_max = doy_max

    def set_features(self, features_to_interpolate, nofill_features, indicator_features, label_columns, include_features):
        """
        Configure time series features for processing
        :param features_to_interpolate: List of column names to interpolate (linearly)
        :param nofill_features: List of column names for which empty cells should be replaced with 0
        :param indicator_features: Dictionary of column names and their corresponding values
        :param label_columns: List of label columns
        :param include_features: List of names of the columns to include in the tensor for the model
        """
        self.features_to_interpolate = features_to_interpolate
        self.nofill_features = nofill_features
        self.indicator_features = indicator_features
        self.label_columns = label_columns
        self.include_features = include_features

    @staticmethod
    def create_indicator_features_parcel(dtp, indicator_features):
        """
        Create indicator columns for a single parcel
        :param dtp: DataFrame with time series of a single parcel
        :param indicator_features: Dictionary of column names and their corresponding values
        :return: dtp with the designated columns converted into indicator columns
        """
        cols_to_remove = []

        for key, val in indicator_features.items():
            for v in val:
                new_col_name = "{}_{}".format(key, v)

                dtp[new_col_name] = (dtp[key] == v).astype(np.uint8)
            cols_to_remove.append(key)

        dtp.drop(columns=cols_to_remove, inplace=True)

        return dtp

    @staticmethod
    def resample_parcel(dtp, doy_min, doy_max,
                        feat_linp_cols, nofill_cols,
                        date_col="ts_date", label_col="label"):
        """
        Resample the time series of a specific parcel
        :param dtp: DataFrame with time series of a single parcel
        :param doy_min: DoY to mark the first data point of the time series
        :param doy_max: DoY to mark the last data point of the time series
        :param feat_linp_cols: List of column names to interpolate (linearly)
        :param nofill_cols: List of column names for which empty cells should be replaced with 0
        :param date_col: Name of the date column
        :param label_col: Name of the label column (not converted into indicator columns yet)
        :return: dtp with resampled data points and gaps which have been interpolated linearly
        """
        # Assume that each parcel has a different ID each year.
        # Extract year and doy.
        dtp.loc[:, "_year"] = dtp[date_col].dt.year
        dtp.loc[:, "_doy"] = dtp[date_col].dt.dayofyear

        resampled = pd.DataFrame(index=range(doy_min, doy_max))
        resampled = resampled.reset_index().rename(columns={"index": "_doy"})
        resampled = resampled.merge(dtp, how="left", on="_doy", suffixes=("_x", "_y"))

        # After the merge, there may be multiple rows with identical _ts_date if, for example, there's an S2 measurement
        # on the same day as an S1 measurement.
        # Join the rows with identical _ts_date.
        # Keep maximum value (better coherence, less cloudy NDVI).
        groups = resampled.groupby(resampled["_doy"], as_index=False)
        resampled = groups.agg("max")

        # Remove _doy, _year columns again.
        resampled = resampled.drop(columns=["_doy", "_year"])

        # Interpolate or fill gaps in columns.
        for c in resampled.columns:
            if c in feat_linp_cols:
                resampled[c].replace(0, np.nan, inplace=True)

                if resampled[c].count() > 1:
                    resampled[c].interpolate(method="slinear", inplace=True)
            # Fill gaps in other columns.
            elif c != label_col:
                # Columns "t" and "dt" are not interpolated, because they help the model to determine days
                # with measurements and interpolated days.
                if c not in nofill_cols:
                    resampled[c].fillna(method="ffill", inplace=True)

                # Optimize RON column types.
                if "_ron_" in c:
                    resampled[c] = resampled[c].fillna(0).astype(np.uint8)

        resampled.fillna(0, inplace=True)

        return resampled

    @staticmethod
    def reshape_sample(dtp, max_len, columns_id_date, columns_label):
        """
        Convert a Pandas DataFrame into Numpy arrays with proper padding.
        :param dtp: Pandas DataFrame with the resampled time series
        :param max_len: Maximum length of the time series (number of DoY)
        :param columns_id_date: List of column names for parcel_id, s1_date (and similar)
        :param columns_label: List of label columns (converted into indicator columns)
        :return: (max_len, data_dim), x, y, dt_ids_times
        where data_dim is the tensor width (number of features),
        x is the input tensor, y the expected results (labels),
        and dt_ids_times contains the values from the parcel_id, s1_date (and similar) columns.
        """
        columns = columns_id_date + columns_label
        data_dim = len([col for col in dtp.columns if col not in columns])

        # Number of id, time columns.
        nit = len(columns_id_date)
        # It's multiclass
        output_size = len(columns_label)

        # It is assumed that the first two columns in the dataframe are the parcel id and date,
        # while the last column is the class label. All columns in between are used as predictor variables.
        _x = np.array(dtp.iloc[:, nit:(nit + data_dim)])
        x = np.pad(_x, [(max_len - _x.shape[0], 0), (0, 0)], mode='constant')
        # Take the last label.
        y = np.array(dtp.iloc[-1, -1-(output_size - 1):], dtype=np.uint8)
        dt_ids_times = np.array(dtp.iloc[:, :nit])

        return (max_len, data_dim), x, y, dt_ids_times

    def go(self, num_parcels_per_iteration):
        """
        Start processing the time series, yielding processed parcel groups
        :param num_parcels_per_iteration: Size of the group of parcels to process at once (used to limit RAM usage)
        :return: Yield index, num_parcels, parcel_ids, x, y
        where index indicates the current progress (first parcel of the group),
        num_parcels indicates the total count to be processed,
        parcel_ids lists the parcel_id values for the current group,
        x the input data and y the expected labels for the group.
        """
        self.log.info("Loading {} ...".format(self.path_input))
        dt = pd.read_csv(self.path_input, sep=";")
        dt.rename(columns={"ts_date": "s1_date"}, inplace=True, errors="ignore")

        # Optimize datatypes.
        dt.loc[:, "s1_date"] = pd.to_datetime(dt["s1_date"], utc=True)
        # for col, dtype in self.feature_dtypes.items():
        #     dt.loc[:, col] = dt[col].astype(dtype)

        parcel_ids = list(dt["parcel_id"].unique())
        f_ids = list(dt["f_id"].unique())

        if num_parcels_per_iteration <= 0:
            num_parcels_per_iteration = len(parcel_ids)

        for i in range(0, len(parcel_ids), num_parcels_per_iteration):
            samples_pid = []
            samples_x = []
            samples_y = []
            samples_fid = []

            num_left = num_parcels_per_iteration
            if i + num_left > len(parcel_ids):
                num_left = len(parcel_ids) - i

            self.log.info(
                "Processing {} ... {} / {} ({:.2f}%)...".format(
                    i, i + num_left, len(parcel_ids),
                    100 * (i + num_left) / len(parcel_ids)
                )
            )
            for j in range(0, num_parcels_per_iteration):
                if i + j >= len(parcel_ids):
                    break

                p_id = parcel_ids[i + j]
                f_id = f_ids[i + j]
                dtp = dt[dt["parcel_id"] == p_id].sort_values("s1_date")

                dtp = self.create_indicator_features_parcel(dtp, self.indicator_features)

                common_columns = [col for col in self.include_features if col in dtp.columns]
                dtp = dtp[common_columns]

                dtp_resampled = self.resample_parcel(
                    dtp, self.ts_doy_min, self.ts_doy_max,
                    self.features_to_interpolate, self.nofill_features,
                    date_col="s1_date"
                )

                input_shape, x, y, dt_ids_times = self.reshape_sample(
                    dtp_resampled, self.ts_doy_max - self.ts_doy_min,
                    ["parcel_id", "s1_date"], self.label_columns
                )

                samples_pid.append(p_id)
                samples_fid.append(f_id)
                samples_x.append(x)
                samples_y.append(y)

            yield i + num_left, len(parcel_ids), samples_pid, samples_fid, np.stack(samples_x), np.stack(samples_y)
