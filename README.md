# RITA-Evaluator #

Crop classification prototype predictor with evaluation, for the RITA project.
This developement was financially supported by the European Regional Development Fund within National Programme for Addressing Socio-Economic Challenges through
R&D (RITA).

Methodology for crop classification developed by KappaZeta Ltd in cooperation with Department of Geography at the University of Tartu. Implementation by KappaZeta Ltd.

> KappaZeta Ltd
>
> Address: Kastani 42, 50410 Tartu, Estonia
>
> Ph: +372 56 669 225
>
> E-mail: info@kappazeta.ee
>
> <https://kappazeta.ee/>

The project (including both the source code as well as data files) is released under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).


## Setup ##

### Setup: System dependencies ###

The project relies on the following system packages:

* git
* git-lfs
* miniconda3
* unzip

### Setup: Conda environment, data preparation ###

1. Clone the repository, and enter the directory.
2. Install Git LFS with

        git lfs install

3. Fetch Git LFS files with

        git lfs pull

4. Decompress the time series and model weights with

        unzip data/timeseries.zip
        unzip model/model.zip

5. Create the `rita-evaluator` Conda environment with

        conda env create -f environment.yml

### Setup: Running ###

1. Activate the Conda environment with

        source activate rita-evaluator

2. Run the script

        python3 rita-eval.py

### Setup: Tear-down ###

While it's possible to just remove the project directory, the Conda environment must be removed separately.
This can be done with the following command:

        conda remove --all --name rita-evaluator

## Directory tree ##

The project root directory contains the following subdirectories with code:

* `evaluation` - confusion matrix and statistics methods
* `processing` - time series processing and prediction
* `utility` - helpers for logging and CSV storage

The following directories are reserved for data files:

* `data` - input time series
* `model` - model weights and metadata
* `results` - prediction and evaluation results

## Input ##

The evaluator takes time series, model weights and model metadata as an input.
The default paths to the time series, model weights and metadata can be overridden with command-line arguments `-i data/timeseries.csv`, `-w model/model.hdf5` or `-m model/metadata.json`, for example.

### Input: Time series ###

Time series are read from `data/timeseries.csv`. The supplied data file contains the _test_ dataset which has been used to evaluate the model after training.
The _test_ dataset only contains labeled parcels which were not used for training.

The data file is expected to be of CSV format with a header row, and `;` delimiter.
The project expects at least the following columns: `parcel_id`, `ts_date`, `label`.
The usage of the other columns is defined by model metadata (depends on the set of columns which were used for training the model).

The columns could be briefly described as follows:

* `set_name` - dataset name, one of the following: `train`, `val`, `test`
* `split_index` - dataset split index for Cross-Validation (unused)
* `tag` - dataset tag, one of the following: `rita_2018`, `rita_2019`
* `parcel_id` - relative parcel ID
* `f_id` - absolute parcel ID, used to reference to other tables
* `ts_date` - timestamp for the time series data point (either S1, S2 or temperature acquisition time)
* `s1_ron` - S1 RON number 1-4, or `ndvi` for an S2 data point or `temperature` for a temperature data point
* `s2_ron` - S2 RON number 1-4, or `coh` for an S1 data point or `temperature` for a temperature data point
* `x_norm_loc`, `y_norm_loc` - normalized geo-coordinates of the parcel centroid
* `soil_type`, `soil_type2`, `soil_type3`, `soil_type4` - soil type at different generalization levels, from the broadest to the most narrow classification
* `cohvh_min`, `cohvh_mean`, `cohvh_median`, `cohvh_max`, `cohvh_stdv` - minimum, mean, median, maximum and standard deviation of VH 6-day coherence
* `cohvv_min`, `cohvv_mean`, `cohvv_median`, `cohvv_max`, `cohvv_stdv` - minimum, mean, median, maximum and standard deviation of VV 6-day coherence
* `vhvv_min`, `vhvv_mean`, `vhvv_median`, `vhvv_max`, `vhvv_stdv` - minimum, mean, median, maximum and standard deviation of s0vh / s0vv
* `s0vh_min`, `s0vh_mean`, `s0vh_median`, `s0vh_max`, `s0vh_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-1 VH sigma-0
* `s0vv_min`, `s0vv_mean`, `s0vv_median`, `s0vv_max`, `s0vv_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-1 VV sigma-0
* `b2_min`, `b2_mean`, `b2_median`, `b2_max`, `b2_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B2
* `b3_min`, `b3_mean`, `b3_median`, `b3_max`, `b3_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B3
* `b4_min`, `b4_mean`, `b4_median`, `b4_max`, `b4_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B4
* `b5_min`, `b5_mean`, `b5_median`, `b5_max`, `b5_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B5
* `b6_min`, `b6_mean`, `b6_median`, `b6_max`, `b6_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B6
* `b7_min`, `b7_mean`, `b7_median`, `b7_max`, `b7_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B7
* `b8_min`, `b8_mean`, `b8_median`, `b8_max`, `b8_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B8
* `b8a_min`, `b8a_mean`, `b8a_median`, `b8a_max`, `b8a_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B8a
* `b11_min`, `b11_mean`, `b11_median`, `b11_max`, `b11_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B11
* `b12_min`, `b12_mean`, `b12_median`, `b12_max`, `b12_stdv` - minimum, mean, median, maximum and standard deviation of Sentinel-2 B12
* `ndvi_min`, `ndvi_mean`, `ndvi_median`, `ndvi_max`, `ndvi_stdv` - minimum, mean, median, maximum and standard deviation of NDVI
* `ndwi_min`, `ndwi_mean`, `ndwi_median`, `ndwi_max`, `ndwi_stdv` - minimum, mean, median, maximum and standard deviation of NDWI
* `ndvire_min`, `ndvire_mean`, `ndvire_median`, `ndvire_max`, `ndvire_stdv` - minimum, mean, median, maximum and standard deviation of NDVIRE
* `tc_wetness_min`, `tc_wetness_mean`, `tc_wetness_median`, `tc_wetness_max`, `tc_wetness_stdv` - minimum, mean, median, maximum and standard deviation of TC Wetness index
* `tc_vegetation_min`, `tc_vegetation_mean`, `tc_vegetation_median`, `tc_vegetation_max`, `tc_vegetation_stdv` - minimum, mean, median, maximum and standard deviation of TC Vegetation index
* `tc_brightness_min`, `tc_brightness_mean`, `tc_brightness_median`, `tc_brightness_max`, `tc_brightness_stdv` - minimum, mean, median, maximum and standard deviation of TC Brightness index
* `misra_yellow_vegetation_min`, `misra_yellow_vegetation_mean`, `misra_yellow_vegetation_median`, `misra_yellow_vegetation_max`, `misra_yellow_vegetation_stdv` - minimum, mean, median, maximum and standard deviation of MISRA Yellow Vegetation index
* `psri_min`, `psri_mean`, `psri_median`, `psri_max`, `psri_stdv` - minimum, mean, median, maximum and standard deviation of PSRI
* `wri_min`, `wri_mean`, `wri_median`, `wri_max`, `wri_stdv` - minimum, mean, median, maximum and standard deviation of WRI
* `temperature_mean` - mean normalized temperature
* `precipitation_sum_mean_3h` - mean normalized precipitation sum from 3 h before the S1 acquisition
* `precipitation_sum_3h` - binary indicator for the presence of a 3 h precipitation sum (1 if present, 0 otherwise)
* `precipitation_sum_mean_24h` - mean normalized precipitation sum from 24 h before the S1 acquisition
* `precipitation_sum_24h` - binary indicator for the presence of a 24 h precipitation sum (1 if present, 0 otherwise)
* `label` - crop class index

### Input: Model weights ###

Model weights are the result of model training, and the weights define the way in which the model responds to input data.
The weights file does not contain model architecture specification nor any metadata on the input datasets and training statistics.

The weights are supplied as an HDF5 file. By default, the `model/model.hdf5` file is stored in Git LFS as a ZIP archive (`model/model.zip`).

### Input: Model metadata ###

Model metadata describes the model architecture, the input datasets used for training, validation and test, the hyperparameters used, etc.

The metadata is supplied as a JSON file. By default, `model/metadata.json` is used.

## Output ##

As a result of a rita-evaluator run, the following files are produced into the `results/` subdirectory:

* `predictions_raw.csv` - prediction results (probabilities for each class) and labels (binary indicators for each class) for each parcel
* `predictions.csv` - predicted class (class with highest probability), label, and the corresponding class names for each parcel
* `confusion_matrix.csv` - parcel counts by predicted class and label. Ideally, all parcels would be on the main diagonal
* `confusion_matrix_normalized.csv` - normalized confusion matrix (class accuracies by predicted class and label). Ideally, the result would be an identity matrix
* `confusion_matrix_plot.png` - visualization of the normalized confusion matrix
* **`weighted_statistics.csv` - weighted F1 score together with precision and recall for full test data**
* **`per_class_statistics.csv` - precision, recall and F1 score per every crop type for full test data**
* `filtered_results.csv` - if there is -filter option with list of f_ids available, the statistic for them outputs separately
* `filtered_correct.csv` - shows all parcels from f_ids list that classified correctly 
* `filtered_errors.csv` - shows all parcels from f_ids list that was misclassified
* `filtered_statistics.csv` - detailed view of all classification for f_id list

## Command-line interface ##

```
usage: rita-eval.py [-h] [-i PATH_INPUT] [-m PATH_METADATA] [-w PATH_WEIGHTS] [-n NUM_PARCELS] [-l LOGFILE_PATH] [-v VERBOSITY]

optional arguments:
  -h, --help            show this help message and exit
  -i PATH_INPUT, --input PATH_INPUT, default="data/timeseries.csv"
                        Path to the time series dataset.
  -m PATH_METADATA, --meta PATH_METADATA, default="model/metadata.json"
                        Path to the model metadata JSON.
  -w PATH_WEIGHTS, --weights PATH_WEIGHTS, default="model/model.hdf5"
                        Path to the model weights.
  -n NUM_PARCELS, --num-parcels NUM_PARCELS, default=1000
                        Number of parcels to predict at once or -1 to predict all at once.
  -l LOGFILE_PATH, --log LOGFILE_PATH
                        Path for a log file, if desired.
  -v VERBOSITY, --verbosity VERBOSITY
                        Level of verbosity (1 - 3).
  -filter FID_FILTER, --filter FID_FILTER,  default="data/filter_fid.txt",
                        F_id of specific interest.
```

Example with default values:

```
python3 rita-eval.py -i data/timeseries.csv -w model/model.hdf5
INFO: RITA.E: RITA evaluator started..
INFO: RITA.E.MODEL: Constructing model..
INFO: RITA.E.MODEL: Using TensorFlow 2.2.0
2020-09-28 11:35:36.704684: I tensorflow/core/platform/cpu_feature_guard.cc:143] Your CPU supports instructions that this TensorFlow binary was not compiled to use: SSE4.1 SSE4.2 AVX AVX2 FMA
2020-09-28 11:35:36.725118: I tensorflow/core/platform/profile_utils/cpu_utils.cc:102] CPU Frequency: 3699850000 Hz
2020-09-28 11:35:36.726265: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x564e13f93730 initialized for platform Host (this does not guarantee that XLA will be used). Devices:
2020-09-28 11:35:36.726308: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Host, Default Version
2020-09-28 11:35:36.726469: I tensorflow/core/common_runtime/process_util.cc:147] Creating new thread pool with default inter op setting: 2. Tune using inter_op_parallelism_threads for best performance.
INFO: RITA.E.TS: Loading data/timeseries.csv ...
INFO: RITA.E.TS: Processing 0 ... 1000 / 5773 (17.32%)...
INFO: RITA.E.TS: Processing 1000 ... 2000 / 5773 (34.64%)...
INFO: RITA.E.TS: Processing 2000 ... 3000 / 5773 (51.97%)...
INFO: RITA.E.TS: Processing 3000 ... 4000 / 5773 (69.29%)...
INFO: RITA.E.TS: Processing 4000 ... 5000 / 5773 (86.61%)...
INFO: RITA.E.TS: Processing 5000 ... 5773 / 5773 (100.00%)...
INFO: RITA.E.Eval: Weighted F1 score: 0.8434440768874513

```

## To be improved ##

The project is still a prototype, and some of the known shortcomings are listed below:

1. Parallel processing is not supported yet (time series resampling, generation of indicator features, etc. is all performed in a single thread). Parallel processing would improve performance several fold.
2. Some parameters are currently still hard-coded (should be added to metadata files, and read from there).
3. RAM consumption has not been optimized yet.