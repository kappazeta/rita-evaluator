# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

# Copyright 2020 KappaZeta Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os


def csv_write(fpath, data, overwrite=False):
    """
    Create a CSV file or append to it
    :param fpath: Path to the file to write to
    :param data: Pandas DataFrame to store
    :param overwrite: Whether to append to (False) or overwrite (True) the file
    """
    """Create a CSV file or append to it if the file exists."""
    if overwrite or not os.path.isfile(fpath):
        data.to_csv(fpath, sep=";", index=False, header=True, mode="w")
    else:
        data.to_csv(fpath, sep=";", index=False, header=False, mode="a")
